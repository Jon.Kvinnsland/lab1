package rockPaperScissors;

import java.text.BreakIterator;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        // Game loop
        while(true) {
            System.out.printf("Let's play round %d\n", roundCounter);
            String humanChoice = userChoice();
            String computerChoice = randomChoice(rpsChoices);
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);
        
            // Check who won
            if(isWinner(humanChoice, computerChoice)) {
                System.out.printf("%s Human wins.\n", choiceString);
                humanScore += 1;
            } else if(isWinner(computerChoice, humanChoice)) {
                System.out.printf("%s Computer wins.\n", choiceString);
                computerScore += 1;
            } else {
                System.out.printf("%s It's a tie.\n", choiceString);
            } System.out.printf("Score: human %s, computer %s \n", computerScore, humanScore);
        
            // Ask if human wants to play again
            String continueAnswer = continuePlaying();
            if(continueAnswer.equals("n")) {
                break;
            }
            roundCounter += 1;
        } System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    // Functions/methods
    public String randomChoice(List<String> rpsChoices) {
        int randomIndex = new Random().nextInt(rpsChoices.size());
        return rpsChoices.get(randomIndex);
    }
    
    public boolean isWinner(String choice1, String choice2) {
        if(choice1.equals("paper")) {
            return (choice2.equals("rock"));
        } else if(choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else;
            return choice2.equals("scissors");
    }
    
    public String userChoice() {
        while(true) {
            //System.out.println("Your choice (Rock/Paper/Scissors)?");
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            //sc.nextLine().toLowerCase();
            
            if(validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            } else;
                System.out.printf("I don't understand %s. Try again", humanChoice);
            }
        }
    
    public String continuePlaying() {
        while(true) {
            System.out.println("Do you wish to continue playing? (y/n)?");
            String continueAnswer = sc.next().toLowerCase();
            if(validateInput(continueAnswer, Arrays.asList("y", "n"))) {
                return continueAnswer;
            } else {
                System.out.printf("I don't understand %s. Try again", continueAnswer); 
            }
        }
    }   
    
    public boolean validateInput(String input, List<String> validInput) {
        input = input.toLowerCase();
        return validInput.contains(input);
    }
}

